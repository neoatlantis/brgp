#!/usr/bin/env python

import json
import os
import sys
import yaml

YAMLPATH = os.path.join(
    os.path.realpath(os.path.dirname(sys.argv[0])), 'src', 'i18n'
)

filelist = os.listdir(YAMLPATH)

i18nJson = {}
for each in filelist:
    if not each.endswith('.yaml'): continue
    filecontent = open(os.path.join(YAMLPATH, each), 'r').read()
    i18nJson[each[:-5]] = yaml.load(filecontent)

sys.stdout.write('module.exports=' + json.dumps(i18nJson))
