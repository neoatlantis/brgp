# -*- coding: utf-8 -*-
"""
Script for building the final single webpage application.

Tasks:
1. Call `uglifyjs` to compress `build/main.js` even more.
2. Parse `build/index.html`, replace linked css and js with content.
"""

import hashlib
import re
import os
import sys
import subprocess

BASEPATH = os.path.realpath(os.path.dirname(sys.argv[0]))
BUILDPATH = os.path.join(BASEPATH, 'build')

def uglifyjs(path):
    cmd = ["uglifyjs", path, '-c', '-m']
    print " ".join(cmd)
    return subprocess.check_output(cmd)

def cssembed(path):
    global BUILDPATH
    srcpath = path
    outpath = os.path.join(BUILDPATH, os.path.basename(srcpath) + '-cssembed')
    subprocess.call([
        'java',
        '-jar',
        'cssembed-0.4.5.jar',
        '-o',
        outpath,
        srcpath
    ])
    return open(outpath).read()


indexlines = open(os.path.join(BUILDPATH, 'index.html'), 'r').read().split('\n')
result = []
for line in indexlines:
    if line.startswith('<link rel="stylesheet"'):
        path = re.search('href="(.+)"', line)
        if path:
            path = path.group(1)
            path = os.path.realpath(os.path.join(BUILDPATH, path))
            css = cssembed(path)
            result.append('<style>%s</style>' % css)
            continue
    if line.startswith('<script src="'):
        path = re.search('src="(.+)"', line)
        if path:
            path = path.group(1)
            path = os.path.realpath(os.path.join(BUILDPATH, path))
            js = uglifyjs(path)
            h = 'sha256-' + hashlib.sha256(js).digest().encode('base64').strip()
            result.append('<script integrity="%s">%s</script>' % (h, js))
            continue
    result.append(line)

open(os.path.join(BUILDPATH, 'index.pack.html'), 'w+').write('\n'.join(result))
