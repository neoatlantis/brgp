BrGP: Browser-based Good Privacy
================================

**BrGP** is my project aiming at providing full OpenPGP functionalities solely
on a browser and possiblely also on one single page. Thanks to `openpgp.js`,
this is now possible without much efforts.

Because `openpgp.js` is still under active development, many features still
needs to be implemented. Also there could be bugs in this project or even
`openpgp.js`, so do not rely on it on death-or-alive situations.

Because of the browser's nature, storing local keyring permanently is not
as easy as native programs. Possible solutions are (1) adapt this single
webpage to browser extensions, so that data storage will survive clearing
browser cache (2) use io.js or alike, to sync the localStorage data onto disk
(3) use some online services (4) your suggestion!


----

BrGP: 基于浏览器的保密程序
==========================

**BrGP** 是我试图在浏览器上用一个单一的页面提供完整的OpenPGP功能而编写的程序。
因为`openpgp.js`的开发，这个目标现在比较容易实现了。

但是由于`openpgp.js`的开发仍然在进行中，有很多功能本程序尚未实现。
此外在本程序或者上游的库中可能存在Bug，不要用本程序做对保密要求很高的工作。

由于浏览器的设计，本程序在本地存储密钥环数据并不如本地应用那样容易。
可能在未来解决这一问题的方案是：
(1)将本程序写成浏览器扩展，这样清空浏览器缓存的时候就不会清除存储的密钥环了。
(2)使用io.js或者类似的方式，开发本地的程序，将密钥环同步到本地磁盘上。
(3)使用在线存储
(4)或者您提出自己的建议！
