BrGP 中文手册
=============

欢迎您使用BrGP!

BrGP是一个密码工具，致力于让您和他人的通信变得更加安全可靠。
这份手册可以让您了解为什么BrGP能这样做，以及如何使用BrGP。

* [BrGP的原理](principles.html)
* [如何使用BrGP](guide.html)
