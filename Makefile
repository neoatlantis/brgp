all: build/index.html

build: build/index.min.html

clean:
	rm -rf build/*
	
build/index.min.html: build/index.pack.html
	html-minifier \
	--minify-css \
	--remove-comments \
	--collapse-inline-tag-whitespace \
	--collapse-whitespace \
	--conservative-collapse \
	build/index.pack.html > build/index.min.html
	
build/docs: docs/*.md docs/header.html docs/footer.html
	mkdir -p build
	rm -rf ./build/docs
	mdown \
            -i "./docs/**/*.md" -o ./build/docs \
            --header "docs/header.html" --footer "docs/footer.html"
	
build/index.pack.html: build/index.html build/main.js build.pack.py
	python build.pack.py

build/index.html: src/index.html src/static/* build/main.js build/docs
	cp src/index.html build/index.html
	cp -r src/static build/

build/main.js: src/*.js src/lib/*.js build/views.js build/res.i18n.js
	browserify src/main.js -o build/main.js

build/views.js: src/views/*.tag
	mkdir -p build
	riot -m src/views build/views.js
	
build/res.i18n.js: src/i18n/*.yaml build.i18n.py
	mkdir -p build
	python build.i18n.py > build/res.i18n.js
