var riot = require('riot'),
    postal = require('./lib/postal');

var $compose$ = postal.channel('compose'),
    $pgp$ = postal.channel('pgp'),
    $ui$ = postal.channel('ui'),
    $notify$ = postal.channel('notify'),
    $external$ = postal.channel('external');
var openpgp = require('./lib/openpgp'),
    openpgpUtil = require('./openpgp.util.js');

function Compose(){
    // `Compose` service controls the backend behaviour for the UI `Compose a
    // Message` section. User may select designated receivers and signers from
    // the `pgp.keyring` section, whose backend will send user's decision per
    // `postal` to this on channel `compose` using an event `cmd:set-keyids`.
    //
    // Remember that until user finally decided to `generate` the ciphertext,
    // we DO NOT CACHE any choosen keyIDs in this backend. Rather, we store
    // them ONLY in UI frontend, on the consideration that otherwise displayed
    // keyIDs may out-of-sync, which will cheat the user.
    //
    // Because we have used `postal.js` as message bus, we have to query for
    // the pgp keys here again, not just getting the key instances passed.

    var self = this;
    var ui = riot.mount('ui-compose')[0];

    $compose$.subscribe('cmd:set-content', function(e){
        ui.update({
            content: e.data,
            id: e.id || '',
        });
    });

    $compose$.subscribe('cmd:set-keyids', function(e){
        ui.update({ 
            encrypt: openpgpUtil.examineKeyFromIDs(e.encrypt),
            sign: openpgpUtil.examineKeyFromIDs(e.sign),
        });
        $ui$.publish('cmd:switch-section', 'compose');
    });

    ui.on('notify', function(e){
        $notify$.publish(e);
    });

    ui.on('viewkey', function(e){
        $pgp$.publish('cmd:keyring-manage', {
            keyid: e.keyid,
            isPrivate: e.isPrivate,
            options: {
                noActions: true,
            },
        });
    });

    ui.on('generate', function(e){
        /* generate ciphertext */

        var error = {};
        function publishResult(message){
            var result = message.armor();
            $external$.publish('evt:result', {
                data: result,
                id: e.id || '',
            });
            ui.update({ result: result });
        }
        
        // prepare public keys for encrypt

        var publicKeys = [];
        var queried = openpgpUtil.queryKeysFromIDs(e.encrypt, true);
        for(var keyid in queried){
            if(null === queried[keyid]){
                $notify$.publish('err:public-key-nonexist', { keyid: keyid });
                error.publickey = keyid;
                return ui.update({ error: error });
            }
            // TODO examine key's availablity for doing encryption
            publicKeys.push(queried[keyid]);
        }

        // prepare private keys for signing

        var privateKeys = [];
        var queried = openpgpUtil.queryKeysFromIDs(e.sign, true);
        var key;
        for(var keyid in queried){
            key = queried[keyid];
            if(null === key || !key.isPrivate()){
                $notify$.publish('err:private-key-nonexist', { keyid: keyid });
                error.privatekey = keyid;
                return ui.update({ error: error });
            }
            if(!key.isDecrypted){ // if not decrypted yet: decrypt this key
                if(!key.decrypt(e.password[keyid])){ // if key decryption fails
                    console.error(e.password);
                    $notify$.publish('err:decrypt-private-key', { key: key });
                    error.password = keyid;
                    return ui.update({ error: error });
                }
            }
            privateKeys.push(key);
        };

        // do processing the data

        var message, clearsign;
        clearsign = e.clearsign && publicKeys.length < 1;
        if(clearsign){
            message = new openpgp.cleartext.CleartextMessage(
                e.content
            );
        } else {
            message = openpgp.message.fromText(e.content);
        }
        
        if(privateKeys.length > 0){ // sign
            try{
                if(clearsign){
                    message.sign(privateKeys);
                } else {
                    message = message.sign(privateKeys);
                }
                console.debug('signed');
            } catch(e){
                console.error('error signing', e);
                return ui.update({ error: e });
            }
        }

        if(publicKeys.length > 0){ // encrypt
            message.encrypt(publicKeys).then(publishResult, function(err){
                console.error('error encrypting', err);
                ui.update({ error: err });
            });
            return;
        }

        publishResult(message);
    });

    return this;
}

var compose = new Compose();
