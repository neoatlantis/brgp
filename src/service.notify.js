require('./lib/notify.js')(global, global.$);
var riot = require('riot'),
    $notify$ = require('./lib/postal').channel('notify'),
    openpgpUtil = require('./openpgp.util.js');

var ui = riot.mount('ui-notify')[0];

$.notify.addStyle('html-displayer', {
    html: "<div data-notify-html />",
    classes: {
        base: {
            "padding": "8px 15px 8px 14px",
            "text-shadow": "0 1px 0 rgba(255, 255, 255, 0.5)",
            "background-color": "#fcf8e3",
            "border": "1px solid #fbeed5",
            "border-radius": "4px",
            "padding-left": "25px",
            "max-width": "80%",
            "word-wrap": "break-word",
            "word-break": "break-word",
            "line-break": "auto",
        },
        error: {
            "color": "#B94A48",
            "background-color": "#F2DEDE",
            "border-color": "#EED3D7",
        },
        success: {
            "color": "#468847",
            "background-color": "#DFF0D8",
            "border-color": "#D6E9C6",
        },
        info: {
            "color": "#3A87AD",
            "background-color": "#D9EDF7",
            "border-color": "#BCE8F1",
        },
    }
});

ui.on('display', function(e){
    var name = e.name, html = e.html;
    var className = 'error';
    if(name.slice(0,3) == "ok:") className = 'success';
    if(name.slice(0,5) == "info:") className = 'info';
    $.notify(html, {
        style: 'html-displayer',
        className: className,
        position: "left bottom",
        clickToHide: true,
        autoHide: true,
        autoHideDelay: 10000,
    }, {
        showAnimation: 'show',
        hideAnimation: 'hide',
    });
});

function showNotify(name, data){
    if(data && data.key){
        data.key = openpgpUtil.extractKeyInfo(data.key);
    }
    ui.update({ name: name, data: data });
}



$notify$.subscribe('#', function(e, env){
    console.info(env.topic, e);
    showNotify(env.topic, e);
});
