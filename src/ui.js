/*
 * Mount and switch sections and tabs
 * ----------------------------------
 *
 * Sections are left-column listed tasks, Tabs are switched inside sections.
 *
 * UI system accepts per `postal.js` commands to switch between sections
 * and tabs.
 */

var ENV = require('./envar');
var riot = require('riot'),
    postal = require('./lib/postal'),
    clipboard = require('clipboard');
require('../build/views.js');
require('./ui.i18n')(require('../build/res.i18n.js'));

var $ui$ = postal.channel('ui');

var ui = riot.mount('body', ENV)[0];
$ui$.subscribe('cmd:switch-section', function(e){
    ui.update({ section: e });
});

$(function(){

    new clipboard('.btn');
    $ui$.publish('evt:init');

});
