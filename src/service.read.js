var riot = require('riot'),
    postal = require('./lib/postal');

var $read$ = postal.channel('read'),
    $pgp$ = postal.channel('pgp'),
    $ui$ = postal.channel('ui'),
    $notify$ = postal.channel('notify');
var openpgp = require('./lib/openpgp'),
    openpgpUtil = require('./openpgp.util.js');

//////////////////////////////////////////////////////////////////////////////

/* Read message related functions: readMessage(), verifyAndUpdatePlaintext() */

function readMessage(e, callback){
    var message, clearsign = false;

    // Load armored text
    try{
        if(e.message.indexOf('-----BEGIN PGP SIGNED MESSAGE-----') >= 0){
            message = openpgp.cleartext.readArmored(e.message);
            clearsign = true;
        } else {
            message = openpgp.message.readArmored(e.message);
        }
    } catch(e){
        $notify$.publish('err:message-unreadable');
        callback({
            messageUnreadable: true,
            details: e,
        });
        return;
    }
    console.debug('read', message);
    
    // See if encrypted
    var encryptionKeyIDs;
    if(!clearsign){
        encryptionKeyIDs = getHexIDs(message.getEncryptionKeyIds());
    } else {
        encryptionKeyIDs = [];
    }
    if(encryptionKeyIDs.length >= 1){ // needs to be decrypted first
        // ---- See if any key is in our database
        var possibleDecryptKeyinfo = 
            openpgpUtil.queryPrivateKeyInfoFromIDs(encryptionKeyIDs);
            // ! Queried may be subkey IDs: these IDs are also provided to
            // and used by UI in asking for passphrases.
        var possibleDecryptKeyIDs = Object.keys(possibleDecryptKeyinfo);
        if(possibleDecryptKeyIDs.length < 1){
            // no key to decrypt this message
            $notify$.publish('err:message-no-decrypt-key');
            return callback({ noDecryptKey: true });
        }
        // ---- See if any key can be decrypted with given passwords
        var passwords = e.password || {};
        var possibleDecryptKeys = openpgpUtil.queryKeysFromIDs(
            possibleDecryptKeyIDs
        );
        var decryptedKeys = [], wrongPassword = {};
        for(var keyID in possibleDecryptKeys){
            if(undefined === passwords[keyID]) continue;
            var key = possibleDecryptKeys[keyID];
            if(key.decrypt(passwords[keyID])){
                decryptedKeys.push(key);
            } else {
                wrongPassword[keyID] = true;
            }
        }
        console.debug('****', decryptedKeys, wrongPassword);
        if(decryptedKeys.length < 1){
            // no keys decrypted with user input
            var uiret = { password: possibleDecryptKeyinfo }, error = null;
            if(Object.keys(wrongPassword).length > 0){
                error = { wrongPassword: wrongPassword };
            }
            return callback(error, uiret);
        }
        // ---- If some keys do decrypted successfully: try decrypt msg
        //      We use raced Promises to try with all usable keys!
        console.debug('decrypted keys', decryptedKeys);
        var race = [];
        decryptedKeys.forEach(function(key){
            race.push(message.decrypt(key));
        });
        Promise.race(race).then(function(decryptedMessage){
            verifyAndUpdatePlaintext(decryptedMessage, callback);
        }, function(error){
            console.log('Decrypt error:', error);
            return callback({ decryptError: error });
        });
        return;
    }

    // If not encrypted, then just goto next step
    verifyAndUpdatePlaintext(message, callback);
}

function verifyAndUpdatePlaintext(plaintextMessage, callback){
    // This function is used for validating a clearsigned message
    // or a decrypted message. It may called directly from `ui.on('read',`,
    // or within a Promise returned by the decryptor. Either way, this
    // function takes the remaining steps for validation(if any) and
    // telling the UI for update.
    var message = plaintextMessage;
    console.log('Plaintext obtained', message);
    var signingKeyIDs = getHexIDs(message.getSigningKeyIds());
    if(signingKeyIDs.length < 1){
        return callback(null, {
            plaintext: message.getText(),
            verifications: {},
        });
    }
    var signingKeysDict = openpgpUtil.queryKeysFromIDs(signingKeyIDs);
    console.debug("Retrieved signing keys", signingKeysDict);
    var signingKeys = [];
    var verifications = [];
    for(var i in signingKeysDict) signingKeys.push(signingKeysDict[i]);
    try{
        var verifications = message.verify(signingKeys);
    } catch(e){
        console.error(e);
    }
    var verifyResult = {};
    verifications.forEach(function(verification){
        var keyID = verification.keyid.toHex();
        verifyResult[keyID] = { valid: verification.valid };
    });
    var verifiedKeys = openpgpUtil.queryKeyInfoFromIDs(
        Object.keys(verifyResult)
    );
    for(var i in verifyResult) verifyResult[i].key = verifiedKeys[i];
    callback(null, {
        plaintext: message.getText(),
        verifications: verifyResult,
    });
}

function getHexIDs(keyIds){
    var ret = [];
    for(var i in keyIds) ret.push(keyIds[i].toHex());
    return ret;
}

/****************************************************************************/

/* service.read */

function Read(){
    var self = this;

    var ui = riot.mount('ui-read')[0];

    $read$.subscribe('cmd:set-content', function(e){
        ui.update({
            content: e.data,
        });
    });

    function getHexIDs(keyIds){
        var ret = [];
        for(var i in keyIds) ret.push(keyIds[i].toHex());
        return ret;
    }


    ui.on('viewkey', function(e){
        $pgp$.publish('cmd:keyring-manage', {
            keyid: e.keyid,
            isPrivate: e.isPrivate,
            options: {
                noActions: true,
            },
        });
    });

    ui.on('read', function(e){
        readMessage(e, function(error, result){
            console.log('callback', arguments);
            var update = result || {};
            if(null != error){
                update.error = error;
            }
            ui.update(update);
        });
    });

    ui.on('download', function(keyid){
        // TODO call `service.pgp.import` to search
    });


    return this;
}

var read = new Read();
