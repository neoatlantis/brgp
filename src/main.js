global.jQuery = require('jquery');
global.$ = global.jQuery;
require('bootstrap');
//require('bootstrap-tagsinput');
require('./static/jstree.min.js');

require('./lib/fetch');

var ENV = require('./envar');
//////////////////////////////////////////////////////////////////////////////

// Start UI manager
require('./ui.js');

// Start backends
require('./back.savefile.js');
require('./back.storage.js');

// Start Adapters
require('./adapter.chrome-extension.js');

// Start Services
require('./service.notify.js');
require('./service.pgp.js');

if(!ENV.popupMode || ENV.popupCompose){
    require('./service.compose.js');
}
if(!ENV.popupMode || ENV.popupRead){
    require('./service.read.js');
}
/*if(!(ENV.popupMode)){
    require('./service.help.js');
}*/
if(ENV.runningAsChromeExtension && ENV.popupPreauth){
    require('./service.preauth.js');
}
