/*
 * Permanent storage solution for `openpgp.memory.js`. Listens on channel
 * $storage$ for data operation events. Also emits an initialization event at
 * program start up.
 */

var postal = require('./lib/postal');
var $storage$ = postal.channel('storage'),
    $external$ = postal.channel('external');
var prefix = require('./openpgp.memory.js').prototype.prefix;
var ENV = require('./envar');

$(function init(){
    if(typeof window !== 'undefined'){
        if(ENV.hasLocalStorage){
            useLocalStorage();
            return;
        } else {
            usePostMessage();
            return;
        }
    }
});

//////////////////////////////////////////////////////////////////////////////

function useLocalStorage(){
    $storage$.subscribe('evt:dump', function(dump){
        var removeItems = [];
        for(var name in window.localStorage){
            if(name.slice(0, prefix.length) != prefix) continue;
            if(!dump[name]) removeItems.push(name);
        }
        for(var name in dump){
            window.localStorage.setItem(name, dump[name]);
        }
        removeItems.forEach(function(i){
            window.localStorage.removeItem(i);
        });
    });

    var init = {};
    for(var name in window.localStorage){
        if(name.slice(0, prefix.length) != prefix) continue;
        init[name] = window.localStorage.getItem(name);
    }
    $storage$.publish('cmd:init', init);
}

function usePostMessage(){
    $storage$.subscribe('evt:dump', function(dump){
        $external$.publish('evt:dump', dump);
    });

    $external$.subscribe('cmd:init', function(e){
        $storage$.publish('cmd:init', e);
    });

    $external$.publish('evt:sync');
}
