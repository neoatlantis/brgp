/*
 * Subservice for `service.pgp.keyring`, providing key management UI.
 */

var ENV = require('./envar');
var riot = require('riot'),
    openpgp = require('./lib/openpgp')
    openpgpUtil = require('./openpgp.util.js'),
    postal = require('./lib/postal');
var $pgp$ = postal.channel('pgp'),
    $notify$ = postal.channel('notify');

function showKeyDetails(keyid, isPrivate, options){
    $('#ui-pgp-keyring-manage').remove();
    $('body').append($('<div>', {id: 'ui-pgp-keyring-manage'}));

    var ui = riot.mount(
        '#ui-pgp-keyring-manage',
        'ui-pgp-keyring-manage',
        ENV
    )[0];

    var keyring = openpgpUtil.getKeyring(), key = false;
    if(isPrivate)
        key = keyring.privateKeys.getForId(keyid, true);
    else
        key = keyring.publicKeys.getForId(keyid, true);
    if(!key) return;

    var keyReport = openpgpUtil.examineKey(key, keyring.publicKeys);
    console.debug('key report', keyReport);
    ui.update({ manageKey: {
        key: keyReport,
        options: options || {}, 
    }});
    
    ui.on('delete', function(){
        $pgp$.publish('cmd:keyring-remove', {
            keyid: keyid,
            isPrivate: isPrivate,
        });
    });

    ui.on('changePassphrase', function(e){
        $pgp$.publish('cmd:keyring-changepassphrase', {
            keyid: keyid,
            oldPassphrase: e.oldPassphrase,
            newPassphrase: e.newPassphrase,
        });
    });

    ui.on('upload', function(e){
        var server = e;
        var armored = openpgpUtil.armorMultipleKeysOfSameType([key], false);
        var hkp = new openpgp.HKP(server);
        function onSuccess(){
            $notify$.publish('ok:publickey-upload', {
                key: key,
                server: server,
            });
        }
        function onError(){
            $notify$.publish('err:publickey-upload', {
                key: key,
                server: server,
            });
        }
        hkp.upload(armored).then(function(e){
            console.log('public key upload', e);
            if(e.ok) return onSuccess();
            onError();
        }, onError);
        $notify$.publish('info:in-progress');
    });
}

$pgp$.subscribe('cmd:keyring-manage', function(e){
    var keyid = e.keyid, isPrivate = e.isPrivate;
    var options = e.options || {};
    showKeyDetails(keyid, isPrivate, options);
});
