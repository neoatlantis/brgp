/* Provides an interface for parsing armored keys and showing user with
 * confirmation for importing.
 *
 * Notice we do not handle actual key importing here. Modification on keyring
 * is done by sending keys over $pgp$ channel, which will be handled by
 * `service.pgp.keyring.js`.
 */


var riot = require('riot'),
    openpgp = require('./lib/openpgp')
    openpgpUtil = require('./openpgp.util.js'),
    postal = require('./lib/postal');
var $pgp$ = postal.channel('pgp'),
    $notify$ = postal.channel('notify');

function pgpImport(){
    var self = this;

    var ui = riot.mount('ui-pgp-import')[0];

    ui.on('fetch', function(e){
        var keyword = e.keyword,
            server = e.server;
        var hkp = new openpgp.HKP(server);
        hkp.lookup({
            query: keyword,
        }).then(function(e){
            console.log('Fetched', e);
            if(!e){
                $notify$.publish('err:no-publickey-on-server', {
                    keyword: keyword
                });
                return ui.update({ done: true });
            }
            ui.trigger('review', e);
        }, function error(e){
            $notify$.publish('err:publickey-download', {
                server: server,
            });
            ui.update({ done: true });
        });
        $notify$.publish('info:in-progress');
    });

    ui.on('review', function(data){
        var result = openpgp.key.readArmored(data).keys || [];
        console.debug('review key:', result);
        var questions = {}, readKey, readKeyInfo;
        // convert to key info
        for(var i in result){
            readKey = result[i];
            readKeyInfo = openpgpUtil.extractKeyInfo(readKey);
            questions[readKeyInfo.keyID] = readKeyInfo;
            questions[readKeyInfo.keyID].armored = readKey.armor();
        }
        // query for existence
        var existenceQuery = openpgpUtil.queryKeyInfoFromIDs(
            Object.keys(questions)
        );
        var existing = false;
        for(var keyid in questions){
            existing = Boolean(existenceQuery[keyid]) &&
                (!(
                    questions[keyid].isPrivate &&
                    existenceQuery[keyid].isPublic
                ));
            questions[keyid].existing = existing;
        }
        // prompt UI for questions
        ui.update({ questions: questions, done: true });
        console.log(questions);
    });

    ui.on('import', function(armoredKeys){
        var armoredKey, key;
        for(var keyid in armoredKeys){
            armoredKey = armoredKeys[keyid];
            try{
                key = openpgp.key.readArmored(armoredKey).keys[0];
                $pgp$.publish('cmd:keyring-add', key);
            } catch(e){
                continue
            }
        }
        ui.update({ clear: true, done: true });
    });

    return this;
}

module.exports = pgpImport;
