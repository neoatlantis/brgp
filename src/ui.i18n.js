var observer, allres, res;

function decideLocale(){
    var tryLocales = [], tryLocale;
    if(navigator.languages){
        tryLocales = navigator.languages;
    } else {
        tryLocales.push(navigator.language);
    }
    for(var i in tryLocales){
        tryLocale = tryLocales[i].toLowerCase();
        if(allres[tryLocale]){
            return tryLocale;
        }
        for(var localeName in allres){
            if(localeName.slice(0,2) == tryLocale.slice(0,2)){
                return localeName;
            }
        }
    }
    return 'default';
}

function initResource(){
    if(!res) res = allres[decideLocale()];
    return res;
}

function updateTranslation(){
    var res = initResource();
    stopObserve();
    $('[data-i18n]').each(function(){
        var id = $(this).attr('data-i18n');
        if(res[id]) $(this).text(res[id].trim());
        $(this).removeAttr('data-i18n');
    });
    startObserve();
}

function startObserve(){
    if(!observer) observer = new MutationObserver(updateTranslation);
    observer.observe(
        $('body')[0],
        {
            subtree: true,
            childList: true,
            attributes: false,
        }
    );
}

function stopObserve(){
    if(!observer) return;
    observer.disconnect();
}

module.exports = function(res){
    allres = res;
    updateTranslation();
}
