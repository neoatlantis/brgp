/*
 * MemoryLocalStore
 * ================
 * Author: NeoAtlantis <aurichalka@gmail.com>
 *
 * This class implements the `LocalStore` API of `openpgp.js`. However instead
 * of directly seeking storage from localStorage, it just store all keys in
 * memory. This is required because for example packed apps in Google Chrome
 * limits the use of localStorage, and it's also useful when stored content has
 * to be synchronized to a remote place, e.g. a server.
 *
 * This class is specific to the project `BrGP`, using `postal.js` to publish
 * asynchronous events on a channel named `storage`.
 *
 * Because this class provides only a in-memory storage, we have to implement
 * other synchornisation mechanism to save and initialize.
 */

var openpgp = require('./lib/openpgp'),
    postal = require('./lib/postal'),
    keyModule = openpgp.key;

var storage = {};
var $storage$ = postal.channel('storage');

$storage$.subscribe('cmd:init', function(data){
    storage = data;
    $storage$.publish('evt:update');
});


function MemoryLocalStore(){
    var self = this;
    
    this.publicKeysItem = this.prefix + this.publicKeysItem;
    this.privateKeysItem = this.prefix + this.privateKeysItem;

    return this;
}


/*
 * Declare the localstore itemnames
 */
MemoryLocalStore.prototype.prefix = 'openpgp-';
MemoryLocalStore.prototype.publicKeysItem = 'public-keys';
MemoryLocalStore.prototype.privateKeysItem = 'private-keys';

/**
 * Load the public keys from HTML5 local storage.
 * @return {Array<module:key~Key>} array of keys retrieved from localstore
 */
MemoryLocalStore.prototype.loadPublic = function() {
    return loadKeys(storage, this.publicKeysItem);
};

/**
 * Load the private keys from HTML5 local storage.
 * @return {Array<module:key~Key>} array of keys retrieved from localstore
 */
MemoryLocalStore.prototype.loadPrivate = function(){
    return loadKeys(storage, this.privateKeysItem);
};

function loadKeys(storage, itemname){
    var itemnamePrefix = itemname + '-',
        itemnamePrefixLen = itemnamePrefix.length;
    var keys = [], key;
    for(var id in storage){
        if(id.slice(0, itemnamePrefixLen) != itemnamePrefix) continue;
        key = keyModule.readArmored(storage[id]);
        if(!key.err){
            keys.push(key.keys[0]);
        }
    }
    return keys;
}

/**
 * Saves the current state of the public keys to HTML5 local storage.
 * The key array gets stringified using JSON
 * @param {Array<module:key~Key>} keys array of keys to save in localstore
 */
MemoryLocalStore.prototype.storePublic = function(keys){
    storeKeys(storage, this.publicKeysItem, keys);
};

/**
 * Saves the current state of the private keys to HTML5 local storage.
 * The key array gets stringified using JSON
 * @param {Array<module:key~Key>} keys array of keys to save in localstore
 */
MemoryLocalStore.prototype.storePrivate = function(keys){
    storeKeys(storage, this.privateKeysItem, keys);
};

function storeKeys(storage, itemname, keys){
    var prefix = itemname + '-', prefixLen = prefix.length;
    var keyDict = {}, removeIDs = [];
    if(!keys.length) keys = [];

    keys.forEach(function(key){
        var name = prefix + key.primaryKey.getKeyId().toHex();
        storage[name] = key.armor();
        keyDict[name] = true;
    });
    for(var name in storage){
        if(name.slice(0, prefixLen) != prefix) continue; // not concerned
        if(!keyDict[name]) removeIDs.push(name);
    }
    for(var i in removeIDs){
        delete storage[removeIDs[i]];
    }
    
    $storage$.publish('evt:dump', storage);
    $storage$.publish('evt:update');
}



module.exports = MemoryLocalStore;
