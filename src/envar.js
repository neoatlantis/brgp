/*
 * Environmental variables detector and setter
 *
 * Returns an object containing a lot of settings, will be used across the
 * program.
 */

var debug = {
    runningAsChromeExtension: true,
};

var dev = false;
//dev = true;
//////////////////////////////////////////////////////////////////////////////

function exp(name, calcFunc){
    if(dev && debug[name] !== undefined)
        var value = debug[name];
    else
        var value = calcFunc();
    Object.defineProperty(module.exports, name, {
        value: value,
        writable: false,
    });
    return value;
}

exp("developingMode", function(){ return dev; });

var runningAsChromeExtension = exp("runningAsChromeExtension", function(){
    try{
        if('chrome-extension:' == window.location.protocol) return true;
        if(window.chrome && chrome.runtime && chrome.runtime.id) return true;
    } catch(e){
    }
    return false;
});

exp("hasLocalStorage", function(){
    try {
        var storage = window['localStorage'],
        x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    } catch(e) {
        return false;
    }
});

function ret(i){
    if(i) return function(){ return true; };
    else return function(){ return false; };
}

(function(){

    var hash = window.location.hash.slice(1);
    exp("popupMode", ret(hash.slice(0, 6) == 'popup-'));
    exp("popupCompose", ret(hash == 'popup-compose'));
    exp("popupRead", ret(hash == 'popup-read'));
    exp("popupPreauth", ret(hash == 'popup-preauth'));

})();

console.log("Environment variables", module.exports);
