var postal = require('./lib/postal'),
    ENV = require('./envar');

var $external$ = postal.channel('external'),
    $compose$ = postal.channel('compose'), // channel of `service.compose`
    $read$ = postal.channel('read');       // channel of `service.read`

if(ENV.runningAsChromeExtension){

    // converts external message posted to us using `window.postMessage` to
    // channel $external$
    window.addEventListener('message', function(e){
        if(window.location.origin != e.origin) return;
        if(!e.data) return;
        var data = e.data.data, name = e.data.event;
        console.log("BrGP Adapter Received Message", e);
        $external$.publish('cmd:' + name, data);
    });

    // listens events sent to $external$
    $external$.subscribe('#', function(data, env){
        if(env.topic.slice(0,4) != 'evt:') return;
        var name = env.topic.slice(4);
        window.parent.postMessage({
            event: name,
            data: data,
        }, window.location.origin);
    });

    if(ENV.popupMode){
        $external$.subscribe('cmd:pgpmessage', function(e){
            var pass = {
                data: e.data,
                id: e.id || '',
            };
            if(ENV.popupCompose) $compose$.publish('cmd:set-content', pass);
            if(ENV.popupRead) $read$.publish('cmd:set-content', pass);
        });
    }

}
