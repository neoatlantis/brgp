var openpgp = require('./lib/openpgp'),
    MemoryLocalStore = require('./openpgp.memory');

function getKeyring(){
    // always get the fresh keyring, forcing loading from localStorage
    return new openpgp.Keyring(new MemoryLocalStore());
}
module.exports.getKeyring = getKeyring;



function examineKey(key, publicKeyring){
    /* Generates a report for examining the key thoroughly.
     * Signatures will be verified when possible.
     */

    var ret = extractKeyInfo(key);
    var validation;

    // Verify primary key
    
    validation = key.verifyPrimaryKey();
    ret.primaryKeyStatus = { 
        valid: (validation === openpgp.enums.keyStatus.valid),
        expired: (validation === openpgp.enums.keyStatus.expired),
        revoked: (validation === openpgp.enums.keyStatus.revoked),
    };

    // overwrite userIds list
    
    ret.userIds = [];
    var user, add;
    for(var i in key.users){
        user = key.users[i];
        if(!user.userId) continue; // if not text, e.g. image data

        add = {};
        add.userid = user.userId.userid;

        // Verify user
        validation = user.verify(key.primaryKey);
        add.validSelfSigned = (validation === openpgp.enums.keyStatus.valid);

        // Verify other certifications(signatures from others)
        add.otherCertifications = [];
        for(var j in user.otherCertifications){
            var cert = user.otherCertifications[j],
                issuerKeyID = cert.issuerKeyId.toHex();
            var issuerKey = false;
            if(publicKeyring)
                issuerKey = publicKeyring.getForId(issuerKeyID, true);
            validation = null;
            if(issuerKey){ // then verify this certification
                // see `https://github.com/openpgpjs/openpgpjs/blob/master/src/key.js`
                try{
                    validation = cert.verify(
                        issuerKey.primaryKey,
                        {userid: user.userId, key: key.primaryKey}
                    );
                } catch(e){
                    validation = null;
                    console.error("Error validating other certification", e);
                }
                //console.debug("**Cert**", cert, issuerKeyID, issuerKey, validation);
            }
            add.otherCertifications.push({
                verified: validation,
                issuer: (issuerKey ?
                    issuerKey.getPrimaryUser().user.userId.userid :
                    null
                ),
                issuerKeyID: (issuerKey ?
                    issuerKey.primaryKey.getKeyId().toHex() :
                    null
                ),
                created: cert.created,
                trustAmout: cert.trustAmount,
                trustLevel: cert.trustLevel,
                notation: cert.notation,
            });
        }

        ret.userIds.push(add);
    }

    // overwrite subkey list
    
    ret.subkeys = [];
    for(var i in key.subKeys){
        var add = {}, item, subKey;
        item = key.subKeys[i];
        subKey = item.subKey;
        add.algorithm = subKey.algorithm;
        add.created = subKey.created;
        add.fingerprint = subKey.getFingerprint();
        validation = item.verify(key.primaryKey);
        add.status = { 
            valid: (validation === openpgp.enums.keyStatus.valid),
            expired: (validation === openpgp.enums.keyStatus.expired),
            revoked: (validation === openpgp.enums.keyStatus.revoked),
        };
        ret.subkeys.push(add);
    }

    return ret;
}
module.exports.examineKey = examineKey;



function extractKeyInfo(from){
    // read important info from an OpenPGP Key Instance

    var ret = {};
    ret.userIds = from.getUserIds();
    ret.isPrivate = from.isPrivate();
    ret.isPublic = from.isPublic();
    ret.primaryUserid = from.getPrimaryUser().user.userId.userid;

    ret.primaryKeyFingerprint = from.primaryKey.getFingerprint();
    ret.primaryKeyAlgorithm = from.primaryKey.algorithm;
    ret.primaryKeyCreated = from.primaryKey.created;

    ret.keyId = from.primaryKey.getKeyId().toHex();
    ret.keyID = ret.keyId;
    ret.userIds = from.getUserIds();
    
    ret.subkeys = [];
    var subkeys = from.getSubkeyPackets();
    for(var i in subkeys){
        var subkey = {};
        subkey.algorithm = subkeys[i].algorithm;
        subkey.created = subkeys[i].created;
        subkey.fingerprint = subkeys[i].getFingerprint();
        ret.subkeys.push(subkey);
    }

    ret.isDecrypted = true;
    if(ret.isPrivate){
        ret.isDecrypted &= from.primaryKey.isDecrypted;
        for(var i in from.subKeys){
            ret.isDecrypted &= from.subKeys[i].subKey.isDecrypted;
        }
    }
    ret.isDecrypted = Boolean(ret.isDecrypted);

    return ret;
}
module.exports.extractKeyInfo = extractKeyInfo;



function queryKeysFromIDs(keyIDs, includeNonexist){
    var keyring = getKeyring(), keyID, query;
    var ret = {};
    for(var i in keyIDs){
        keyID = keyIDs[i];
        query = keyring.getKeysForId(keyID, true);
        if(!query || query.length < 1){
            if(includeNonexist) ret[keyID] = null;
            continue;
        }
        var collected = false;
        for(var j in query){
            if(query[j].isPrivate()){ // return private key with priority
                ret[keyID] = query[j];
                collected = true;
                break;
            }
        }
        if(!collected) ret[keyID] = query[0]; // otherwise just the first key
    }
    return ret;
}
module.exports.queryKeysFromIDs = queryKeysFromIDs;



function queryKeyInfoFromIDs(keyIDs){
    var keys = queryKeysFromIDs(keyIDs);
    for(var i in keys) keys[i] = extractKeyInfo(keys[i]);
    return keys;
}
module.exports.queryKeyInfoFromIDs = queryKeyInfoFromIDs;



function queryPrivateKeyInfoFromIDs(keyIDs){
    var keys = queryKeyInfoFromIDs(keyIDs), ret = {};
    for(var i in keys) if(keys[i].isPrivate) ret[i] = keys[i];
    return ret;
}
module.exports.queryPrivateKeyInfoFromIDs = queryPrivateKeyInfoFromIDs;



function examineKeyFromIDs(keyIDs){
    var keys = queryKeysFromIDs(keyIDs);
    for(var i in keys) keys[i] = examineKey(keys[i]);
    return keys;
}
module.exports.examineKeyFromIDs = examineKeyFromIDs;



function armorMultipleKeysOfSameType(keys, isPrivate){
    var array = [], packetList, exportingKey;
    isPrivate = Boolean(isPrivate);
    packetList = new openpgp.packet.List();
    for(var i in keys){
        exportingKey = keys[i];
        if(!isPrivate) exportingKey = exportingKey.toPublic();
        if(isPrivate ^ exportingKey.isPrivate()) continue;

        exportingKey.toPacketlist().forEach(function(packet){
            packetList.push(packet);
        });
    }
    var type = (isPrivate ? 
        openpgp.enums.armor.private_key : 
        openpgp.enums.armor.public_key
    );
    return openpgp.armor.encode(type, packetList.write()); 
}
module.exports.armorMultipleKeysOfSameType = armorMultipleKeysOfSameType;
