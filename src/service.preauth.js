var riot = require('riot'),
    postal = require('./lib/postal');

var $compose$ = postal.channel('compose'),
    $pgp$ = postal.channel('pgp'),
    $ui$ = postal.channel('ui'),
    $notify$ = postal.channel('notify'),
    $external$ = postal.channel('external');
var openpgp = require('./lib/openpgp'),
    openpgpUtil = require('./openpgp.util.js');
var ENV = require('./envar');

function Preauth(){
    // `Preauth` service turns the current instance of BrGP as a semi or fully
    // automated web page, which processes incoming requests and returns
    // results. This design is merely for the mode running as a browser
    // extension, for cooperating with applications that needs frequent PGP
    // operations.

    var self = this;
    var ui = riot.mount('ui-preauth')[0];

    $pgp$.subscribe('evt:keyring-list', function(list){
        ui.update({ privateKeys: list });
    }).once();
    $pgp$.publish('cmd:keyring-list', { private: true, public: false });

    return this;
}

var preauth = new Preauth();
