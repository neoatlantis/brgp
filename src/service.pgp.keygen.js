var riot = require('riot'),
    openpgp = require('./lib/openpgp'),
    postal = require('./lib/postal');
var $pgp$ = postal.channel('pgp'),
    $notify$ = postal.channel('notify');

function pgpKeygen(){
    var self = this;
    riot.observable(this);

    var ui = riot.mount('ui-pgp-keygen')[0];

    function onSubmission(el){
        var options = {
            keyType: 1, // RSA encrypt + sign
            numBits: parseInt(el.bits),
            userIds: [el.userid],
            passphrase: el.passphrase,
        };
        openpgp.generateKey(options).then(function(key){
            $notify$.publish("ok:generating-key", { key: key.key });
            $pgp$.publish('cmd:keyring-add', key.key); 
            ui.update({ generating: false, ok: true });
        }, function(){
            $notify$.publish("err:generating-key");
            ui.update({ generating: false, ok: false });
        });
    }


    ui.on('submission', onSubmission);

    return this;
}

module.exports = pgpKeygen;
