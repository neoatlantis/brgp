<ui-pgp>

    <ul class="nav nav-tabs" if={ !opts.popupMode }>
        <li role="presentation" class="active"><a href="#pgpkeyring" data-toggle="pill" data-i18n="pgp.all">All</a></li>
        <li role="presentation"><a href="#pgpkeygen" data-toggle="pill" data-i18n="pgp.generate">Generate</a></li>
        <li role="presentation"><a href="#pgpimport" data-toggle="pill" data-i18n="pgp.import">Import</a></li>
    </ul>


    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="pgpkeyring">
            <ui-pgp-keyring></ui-pgp-keyring>
        </div>
        <div role="tabpanel" class="tab-pane" id="pgpkeygen">
            <ui-pgp-keygen></ui-pgp-keygen>
        </div>
        <div role="tabpanel" class="tab-pane" id="pgpimport">
            <ui-pgp-import></ui-pgp-import>
        </div>
    </div>


</ui-pgp>
