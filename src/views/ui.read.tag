<ui-read>

<!-- ********** PANEL: Message input ********** -->

<div class="panel panel-default" id="read-input">
    <div class="panel-heading">
        <h4 class="panel-title" data-i18n="message-to-read" if={ !password }>
            Message to read? 
        </h4>
        <h4 class="panel-title" data-i18n="we-need-your-password" if={ password }>
            We need your password...
        </h4>
    </div>
    <div class="panel-body" if={ !password }>
        <textarea 
            name="message" 
            class="form-control"
        ></textarea>
    </div>
    <div class="panel-body" if={ password } data-i18n="read.can-decrypt-with-following-keys">
        You can decrypt the message with any of the following private key(s).
        Enter the password for the key you want to use.
    </div>
    <ul class="list-group" if={ password }>
        <li each={ id, key in password } data-id="pwd-{ id }" class="list-group-item">
            { key.primaryUserid }
            <input data-id="{ id }" type="password" class="form-control" />
            <button type="button" data-viewkey="prv-{ id }" class="btn btn-link" data-i18n="detail">Detail</button>
            <div if={ error.wrongPassword[id] } class="alert alert-danger" data-i18n="invalid-password">Invalid password.</div>
        </li>
    </ul>
    <div class="panel-footer">
        <button type="button" onclick={ read } class="btn btn-primary">
            <span if={ password } data-i18n="continue-reading">Continue Reading</span>
            <span if={ !password } data-i18n="read">Read</span>
        </button>
        <button type="button" onclick={ clear } class="btn btn-default" data-i18n="clear-all" if={ !password }>
            Clear All
        </button>
        <button class="btn btn-default" data-i18n="back" onclick={ goback } if={ password }>Back</button>
    </div>
</div>

<!-- ********** PANEL: Plaintext ********** -->

<div class="panel panel-success" id="read-plaintext">
    <div class="panel-heading">
        <h4 class="panel-title" data-i18n="message-read-successful">
            Message read successful
        </h4>
    </div>
    <ul class="list-group" if={ Object.keys(verifications).length < 1 }>
        <li class="list-group-item list-group-item-warning">
            <span data-i18n="read.message-not-signed">This message is not signed.</span>
            You should <strong>not</strong> trust it's been sent from anyone as said.
        </li>
    </ul>
    <ul class="list-group" if={ Object.keys(verifications).length > 0 }>
        <li class="list-group-item" data-i18n="read.message-signed">
            This message is signed by one or more key(s).
            Verification results are shown below.
        </li>
        <li 
            each={ id, verification in verifications } 
            class="list-group-item { verification.valid ? 'list-group-item-success' : 'list-group-item-danger' }"
        >
            <span if={ verification.key }>
                { verification.key.primaryUserid }
                <strong if={ verification.valid } data-i18n="read.verification-successful">
                    Verification successful.
                </strong>
                <strong if={ !verification.valid } data-i18n="read.message-may-tampered">
                    Verification FAILED! Message may have been tampered.
                </strong>
                <button type="button" data-viewkey="pub-{ verification.key.keyID }" class="btn btn-link btn-xs" data-i18n="detail">Detail</button>
            </span>
            <span if={ !verification.key }>
                [<strong>{ id }</strong>]: Signer public key with this ID is unknown to us. Cannot verify.
                <button onclick={ download } data-keyid="{ id }" class="btn btn-link btn-xs">Search this public key from Internet.</button>
            </span>
        </li>
    </ul>
    <div class="panel-body">
        <textarea id="read-result-textarea" name="result-display" class="form-control" readonly="readonly"> </textarea>
    </div>
    <div class="panel-footer">
        <button class="btn btn-success" data-clipboard-target="#read-result-textarea" data-i18n="copy-to-clipboard">Copy to Clipboard</button>
        <button class="btn btn-default" data-i18n="back" onclick={ goback }>Back</button>
    </div>
</div>




<!-- ********************************************************************* -->
<script>

var root = this.root,
    self = this;

this.error = {};

function switchToInput(){
    $(root).find('#read-plaintext').hide();
    $(root).find('#read-input').show();
}

function switchToPlaintext(){
    $(root).find('#read-input').hide();
    $(root).find('#read-plaintext').show();
}

this.on('mount', function(){
    switchToInput();
});

this.on('update', function(e){
    console.log("service.read-ui", e);
    if(e && undefined !== e.plaintext){
        if(!e.error) this.error = {};
        this.password = null;
        switchToPlaintext();
    }
    if(e && e.content){
        $(this.root).find('[name="message"]').text(e.content);
        switchToInput();
    }
});

this.on('updated', function(){
    if(this.plaintext){
        $(root).find('[name="result-display"]').text(this.plaintext);
    }
    $(root).find('button[data-viewkey]').off('click').click(function(e){
        var itemid = $(this).attr('data-viewkey'),
            type = itemid.slice(0,3),
            keyid = itemid.slice(4);
        self.trigger('viewkey', {
            keyid: keyid,
            isPrivate: (type === 'prv'),
        });
    });
}); 

read(el){
    var password = {};
    $(this.root).find('input[type="password"]').each(function(){
        password[$(this).attr("data-id")] = $(this).val();
    });
    var submission = {
        message: this.message.value,
        password: password,
    };
    console.debug(submission);
    this.trigger('read', submission);
}

clear(){
    this.message.value = '';
}

goback(){
    this.error = {};
    this.password = null;
    switchToInput();
}

download(el){
    var keyid = $(el.srcElement).attr('data-keyid');
    this.trigger('download', keyid);
}

</script>
</ui-read>
