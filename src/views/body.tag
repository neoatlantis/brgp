<body>
<div class="{ opts.popupMode ? 'container-fluid': 'container' }">

<div if={ opts.developingMode }> <strong>Developing Mode</strong> </div>

<div class="row" if={ !opts.popupMode || opts.popupPreauth }>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1>BrGP <small data-i18n="browser-based-good-privacy">Browser-based Good Privacy</small></h1>
    </div>
</div>
<div class="row">

    <div
        class="col-xs-12 col-sm-12 { opts.popupMode?'col-md-12 col-lg-12':'col-md-3 col-lg-3' }"
        if={ !(opts.popupRead || opts.popupPreauth) }
    >
        <ul class="nav nav-pills { !opts.popupMode?'nav-stacked':'' }">
            <li role="presentation" class="active">
                <a href="#pgp" data-toggle="pill" if={ !opts.popupMode } data-i18n="key-management">Key Management</a>
                <a href="#pgp" data-toggle="pill" if={ opts.popupMode }>
                    1. <span data-i18n="choose-recipient-signer">Choose recipient and/or signer</span>
                </a>
            </li>
            <li role="presentation" if={ !(opts.popupMode && !opts.popupCompose) }>
                <a href="#compose" data-toggle="pill" if={ !opts.popupMode } data-i18n="compose-a-message">Compose a Message</a>
                <a href="#compose" data-toggle="pill" if={ opts.popupMode }>2. <span data-i18n="confirm-and-generate">Confirm and generate</span></a>
            </li>
            <li role="presentation" if={ !(opts.popupMode && !opts.popupRead) }>
                <a href="#read" data-toggle="pill" data-i18n="read-a-message">Read a Message</a>
            </li>
            <li role="presentation" if={ !opts.popupMode }>
                <a href="#help" data-toggle="pill" data-i18n="help">Help</a>
            </li>
        </ul>
    </div>

    <div class="col-xs-12 col-sm-12 { opts.popupMode?'col-md-12 col-lg-12':'col-md-9 col-lg-9' }">

        <div
            class="tab-content"
            if={ !opts.popupMode || opts.popupCompose }
        >
            <div role="tabpanel" class="tab-pane active" id="pgp">
                <ui-pgp></ui-pgp>
            </div>
            <div role="tabpanel" class="tab-pane" id="compose">
                <ui-compose></ui-compose>
            </div>
            <div role="tabpanel" class="tab-pane" id="read">
                <ui-read></ui-read>
            </div>
            <div role="tabpanel" class="tab-pane" id="help">
                <ui-help></ui-help>
            </div>
        </div>

        <div 
            class="tab-content"
            if={ opts.popupRead }
        >
            <ui-read></ui-read>
            <div class="hidden">
                <ui-pgp></ui-pgp>
                <ui-compose></ui-compose>
            </div>
        </div>
        
        <div 
            class="tab-content"
            if={ opts.popupPreauth }
        >
            <ui-preauth></ui-preauth>
            <div class="hidden">
                <ui-read></ui-read>
                <ui-pgp></ui-pgp>
                <ui-compose></ui-compose>
            </div>
        </div>

    </div>
</div></div>

<ui-notify></ui-notify>



<script>

this.section = "pgp";
this.on("update", function(e){
    if(e && e.section){
        $(this.root).find('a[href="#' + this.section + '"]').tab('show');
    }
});

</script>
</body>
