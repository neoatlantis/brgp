<ui-pgp-keygen>

    <form class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label" data-i18n="username">Username</label>
            <div class="col-sm-9">
                <input class="form-control" name="username" type="text" />
            </div>
            <div if={ err.userid_invalid }>
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <p class="help-block" data-i18n="pgp.keygen.invalid-user-id">Invalid User ID.</p>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" data-i18n="email">Email</label>
            <div class="col-sm-9">
                <input class="form-control" name="email" type="text" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" data-i18n="key-security-bits">Key Security Bits</label>
            <div class="col-sm-9">
                <select class="form-control" name="bits">
                    <option value="2048" data-i18n="pgp.keygen.key-bits-2048">2048 - medium security</option>
                    <option value="4096" data-i18n="pgp.keygen.key-bits-4096">4096 - high security</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" data-i18n="enter-passphrase">Enter Passphrase</label>
            <div class="col-sm-9">
                <input class="form-control" name="passphrase" type="password" />
            </div>
            <div if={ err.passphrase_too_short }>
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <p class="help-block" data-i18n="pgp.keygen.passphrase-too-short"> Passphrase too short.  </p>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label" data-i18n="repeat-passphrase">Repeat Passphrase</label>
            <div class="col-sm-9">
                <input class="form-control" name="passphrase2" type="password" />
            </div>
            <div if={ err.passphrase_not_match }>
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <p class="help-block" data-i18n="pgp.keygen.passphrases-not-match"> Passphrases not match.  </p>
                </div>
            </div>
        </div>

        <div>
            <button class="btn btn-primary" type="button" onclick={ submit } disabled={ generating } data-i18n="pgp.keygen.generate">
                Generate
            </button>
        </div>

    </form>

<script>

this.on('update', function(e){
    if(e){
        if(false === e.generating && true === e.ok){
            $(this.root).find('form')[0].reset();
        }
    }
});

this.err = {
    userid_invalid: false,
    passphrase_too_short: false,
    passphrase_not_match: false,
}
this.generating = false;

submit(el){
    this.err.userid_invalid = (this.username.value.length < 3);
    this.err.passphrase_too_short = (this.passphrase.value.length < 5);
    this.err.passphrase_not_match = (
        this.passphrase.value != this.passphrase2.value
    );
    for(var i in this.err){ if(this.err[i]) return; }
    
    var req = {
        userid: {
            name: this.username.value,
            email: this.email.value,
        },
        bits: this.bits.value,
        passphrase: this.passphrase.value,
    };

    this.generating = true;
    this.privateKey = null;
    this.publicKey = null;

    this.trigger("submission", req);
}

</script>
</ui-pgp-keygen>
