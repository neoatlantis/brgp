<ui-pgp-import>


<div class="panel-group" role="tablist" aria-multiselectable="true">

    <!-- ********** PANEL: Keys input ********** -->

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="import-keys-head">
            <h4 class="panel-title">
                <a 
                    role="button"
                    data-toggle="collapse"
                    data-parent="#import"
                    href="#import-keys"
                    aria-expanded="true"
                    aria-controls="import-keys"
                    data-i18n="enter-the-keys"
                >
                    Enter the keys.
                </a>
            </h4>
        </div>
        <div id="import-keys" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="import-keys-head">
            <div class="panel-body">
                <textarea 
                    name="data" 
                    class="form-control"
                ></textarea>
                <button type="button" onclick={ review } class="btn btn-primary" disabled={ working } data-i18n="import">Import</button>
            </div>
        </div>
    </div>

    <!-- ********** PANEL: Key fetch from server ********** -->

    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="import-fetch-head">
            <h4 class="panel-title">
                <a 
                    role="button"
                    data-toggle="collapse"
                    data-parent="#import"
                    href="#import-fetch"
                    aria-expanded="true"
                    aria-controls="import-fetch"
                    data-i18n="or-fetch-key-from-server"
                >
                    Or fetch a key from server
                </a>
            </h4>
        </div>
        <div id="import-fetch" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="import-fetch-head">
            <div class="panel-body">
                <form onsubmit={ fetchAndReview } class="form">
                    <div class="form-group">
                        <label data-i18n="pgp.import.search-keywords-below">To search a key from key server, input keywords below:</label>
                        <input type="text" class="form-control" name="keyword" />
                    </div>
                    <div class="form-group">
                        <label data-i18n="server">Server</label>
                        <select class="form-control" name="keyserver">
                            <option value="https://pgp.mit.edu">https://pgp.mit.edu</option>
                            <option value="https://sks-keyservers.net">https://sks-keyservers.net</option>
                            <option value="https://pgp.key-server.io">https://pgp.key-server.io</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary" disabled={ working } data-i18n="search">Search</button>
                </form>
            </div>
        </div>
    </div>

    <!-- ********** PANEL: Confirm ********** -->

    <div class="panel panel-success" if={ Boolean(questions) }>
        <div class="panel-heading" role="tab" id="import-confirm-head">
            <h4 class="panel-title">
                <a 
                    role="button"
                    data-toggle="collapse"
                    data-parent="#import"
                    href="#import-confirm"
                    aria-expanded="true"
                    aria-controls="import-confirm"
                    data-i18n="confirm-importation"
                >
                    Confirm the importation
                </a>
            </h4>
        </div>
        <div id="import-confirm" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="import-confirm-head">
            <div class="panel-body" data-i18n="pgp.import.we-have-extracted">
                We have extracted following keys.
                Confirm the keys you're going to import by checking on the keys.
            </div>
            <ul class="list-group">
                <li 
                    each={ id, key in questions } 
                    class="list-group-item"
                >
                    <label>
                        <input type="checkbox" checked="{ !key.existing }" value="{ id }"/> 
                        <div each="{ userId, i in key.userIds }">{ userId }</div>
                    </label>
                    <span if={ key.existing }>(<span data-i18n="key-already-exists">Key already exists</span>)</span>
                </li>
            </ul>
            <div class="panel-body">
                <button type="button" name="confirm" class="btn btn-primary" onclick={ confirm } data-i18n="confirm">Confirm</button>
                <button type="button" class="btn btn-default" onclick={ cancel } data-i18n="cancel">Cancel</button>
            </div>
        </div>
    </div>

</div>

<!-- ********************************************************************* -->

<script>

var self = this;

this.questions = false;
this.working = false;

review(evt){
    evt.preventDefault();
    this.working = true;
    var data = $(this.data).val();
    this.trigger('review', data);
    return false;
}

fetchAndReview(evt){
    evt.preventDefault();
    this.working = true;
    var keyword = $(this.keyword).val(),
        keyserver = $(this.keyserver).val();
    this.trigger('fetch', {
        keyword: keyword,
        server: keyserver,
    });
    return false;
}

confirm(){
    this.working = true;
    var selectedKeyIDs = [], selectedKeys = {}, keyID;
    $(this.root).find('#import-confirm').find('input:checked').each(function(){
        selectedKeyIDs.push($(this).val());
    });
    for(var i=0; i<selectedKeyIDs.length; i++){
        keyID = selectedKeyIDs[i];
        selectedKeys[keyID] = this.questions[keyID].armored || undefined;
    }
    this.trigger('import', selectedKeys);
}

cancel(){
    this.questions = false;
}

this.on('update', function(e){
    if(e && e.clear){
        $(this.root).find('textarea').val('');
        this.questions = false;
    }
    if(e && e.done) this.working = false;
});


</script>
</ui-pgp-import>
