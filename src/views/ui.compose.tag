<ui-compose>

<div class="panel panel-default" id="compose-editor">
    <div class="panel-heading">
        <h4 class="panel-title" data-i18n="compose-a-message">
            Compose message
        </h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12">
                <label data-i18n="recipients">Recipients</label>
                <span
                    if={ Object.keys(_encrypt).length < 1 }
                    data-i18n="compose.no-recipients-defined"
                >
                    Currently no recipients defined. The message
                    would not be encrypted.  Go to "Key Management"
                    and add some public keys here.
                </span>

                <span if={ Object.keys(_encrypt).length > 0 }>
                    <span
                        each={ id, key in _encrypt }
                        data-id="enc-{ id }"
                        class="userid-tag"
                    >
                        <span 
                            data-viewkey="pub-{ id }"
                            class="{ !key.primaryKeyStatus.valid ? 'invalid': '' }"
                        >
                            { key.primaryUserid }
                        </span>
                        <span if={ !key.primaryKeyStatus.valid } onclick={ clickUpdate }>
                            <input type="checkbox" id="enc-{ id }-warn" name="warning" checked/>
                            <label for="enc-{ id }-warn" data-i18n="warning-invalid-key">
                                Warning: Invalid key!
                            </label>
                        </span>
                        <button type="button" data-remove="enc-{ id }" data-i18n="remove">Del</button>
                    </span>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <label data-i18n="signers">Signers</label>
                <span
                    if={ Object.keys(_sign).length < 1 }
                    data-i18n="compose.no-signers-defined"
                >
                    Currently no signers defined. The message would not
                    be signed. Go to "Key Management" and add some
                    private keys here.
                </span>
                
                <span if={ Object.keys(_sign).length > 0 }>
                    <span
                        each={ id, key in _sign }
                        data-id="sig-{ id }"
                        class="userid-tag"
                    >
                        <span data-viewkey="prv-{ id }">{ key.primaryUserid }</span>
                        <input type="password" data-id="sig-{ id }-pwd" placeholder="Password" />
                        <button type="button" data-remove="sig-{ id }" data-i18n="remove">Del</button>
                    </span>
                </span>
            </div>
        </div>
    
        <div class="row">
            <div class="col-sm-12">
                <textarea name="content" class="form-control" disabled={ generating }></textarea>
            </div>
        </div>
    </div>
        
    <div class="panel-footer">
        <div if={ generating } data-i18n="generating">
            Generating...
        </div>
        <div if={ !generating }>
            <div if={ !_shouldSign && !_shouldEncrypt } data-i18n="compose.no-recipient-nor-signer">
                Neither recipient nor signer choosen, cannot do anything.
            </div>
            <div if={ _shouldSign || _shouldEncrypt }>
                <button
                    type="button"
                    class="btn btn-primary"
                    onclick={ submit }
                    if={ _shouldEncrypt }
                >
                    <span if={ _shouldSign } data-i18n="sign-and-encrypt">Sign & Encrypt</span>
                    <span if={ !_shouldSign } data-i18n="encrypt-only">Encrypt Only / No signature</span>
                </button>
                <span if={ !_shouldEncrypt }>
                    <button
                        type="button"
                        class="btn btn-primary"
                        onclick={ submitClearsign }
                        title="Sign without encryption. You text will be left clear for read as it is, so people will quickly get the content."
                        data-i18n="sign-with-cleartext"
                    >
                        Sign with human-readable cleartext / No encryption
                    </button>
                    <button
                        type="button"
                        class="btn btn-default"
                        onclick={ submit }
                        title="Sign without encryption. Your text will be compressed and encoded, requiring others using PGP to read and verify."
                        data-i18n="sign-with-encoded-text"
                    >
                        Sign with encoded text / No encryption
                    </button>
                </span>
                <button type="button" onclick={ clear } class="btn btn-default pull-right" data-i18n="clear-all">
                    Clear All
                </button>
            </div>
        </div>
    </div>
</div>


<div class="panel panel-default" id="compose-result">
    <div class="panel-heading">
        <h4 class="panel-title" data-i18n="get-the-result">
            Get the result
        </h4>
    </div>
    <div class="panel-body">
        <textarea
            id="compose-result-textarea"
            name="result-display" 
            class="form-control"
            readonly="readonly"
        ></textarea>
    </div>
    <div class="panel-footer">
        <button
            class="btn btn-success"
            data-clipboard-target="#compose-result-textarea"
            data-i18n="copy-to-clipboard"
        >Copy to Clipboard</button>
        <button
            class="btn btn-default"
            data-i18n="back"
            onclick={ goback }
        >Back</button>
    </div>
</div>



<script>

var self = this;
var root = this.root;

this.generating = false;
this.result = '';
this._encrypt = {};
this._sign = {};
this._error = {};
this._shouldSign = false;
this._shouldEncrypt = false;

function switchToResult(){
    $(self.root).find('#compose-editor').hide();
    $(self.root).find('#compose-result').show();
}

function switchToContent(){
    $(self.root).find('#compose-editor').show();
    $(self.root).find('#compose-result').hide();
    self.result = '';
    self.update();
}

this.on('mount', function(){
    switchToContent();
});

this.on('update', function(e){
    if(e && (e.encrypt || e.sign)){
        switchToContent();
        if(e.encrypt){
            for(var i in e.encrypt) this._encrypt[i] = e.encrypt[i];
        }
        if(e.sign){
            for(var i in e.sign) this._sign[i] = e.sign[i];
        }
    }
    if(e && (e.result || e.error)){
        this.generating = false;
        if(e.error){
            this._error.password = e.error.password || '';
            if(e.error.publickey) delete this._encrypt[e.error.publickey];
            if(e.error.privatekey) delete this._sign[e.error.privatekey];
            switchToContent();
        }
        if(e.result){
            this._error = {};
            switchToResult();
        }
    }
    if(e && e.content){
        // TODO do not overwrite existing textarea
        $(this.root).find('[name="content"]').text(e.content);
        this._id = e.id;
    }
    this._shouldSign = !$.isEmptyObject(this._sign);
    this._shouldEncrypt = !$.isEmptyObject(this._encrypt);
});

this.on('updated', function(e){
    $(root).find('[data-remove]').off('click').click(function(e){
        var itemid = $(this).attr('data-remove'),
            group = itemid.slice(0,3),
            keyid = itemid.slice(4);
        if('enc' == group) delete self._encrypt[keyid];
        if('sig' == group) delete self._sign[keyid];
        switchToContent();
    });
    $(root).find('[data-viewkey]').off('click').click(function(e){
        var itemid = $(this).attr('data-viewkey'),
            type = itemid.slice(0,3),
            keyid = itemid.slice(4);
        self.trigger('viewkey', {
            keyid: keyid,
            isPrivate: (type === 'prv'),
        });
    });
    $(root).find('[name="result-display"]').val(self.result.trim());
});

function doSubmit(clearsign){
    if($(root).find('[name="warning"]:checked').length > 0){
        self.trigger('notify', 'err:compose-editor-warnings-not-cleared');
        return false;
    }

    var password = {}, clearsign = clearsign || false;
    var submission = {
        id: this._id || '',
        content: $(root).find('[name="content"]').val(),
        encrypt: Object.keys(self._encrypt),
        sign: Object.keys(self._sign),
        clearsign: clearsign,
    };
    for(var keyid in self._sign){
        password[keyid] = 
            $(root).find('input[data-id="sig-' + keyid + '-pwd"]').val();
    }
    submission.password = password;

    setTimeout((function(submission){
        return function(){
            self.trigger('generate', submission);
        }
    })(submission), 5);
    return true;
}

submit(el){
    this.generating = doSubmit(false);
}

submitClearsign(el){
    this.generating = doSubmit(true);
}

goback(){
    switchToContent();
}

clear(){
    this.result = '';
    this._encrypt = {};
    this._sign = {};
    this._error = {};
    this._shouldSign = false;
    this._shouldEncrypt = false;
    $(root).find('[name="content"]').val('');
}

</script>
</ui-compose>
