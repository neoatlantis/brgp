<ui-pgp-keyring-export>

<div class="modal fade" tabindex="-1" role="dialog" name="dialog-export">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" data-i18n="key-export">Key Export</h4>
      </div>
      <div class="modal-body" if={ publicKeyArmored }>
        <p data-i18n="pgp.keyring.export.public-keys-exported-below">
            The public key(s) you've choosen are exported below.
            You can share them freely in your own way.
        </p>
        <textarea
            name="result-display"
            class="publickey form-control"
            readonly="readonly"
        >{ publicKeyArmored }</textarea>
      </div>
      <div class="modal-body" if={ privateKeyArmored }>
        <p data-i18n="pgp.keyring.export.private-keys-exported-below">
            The private key(s) you've choosen are exported below.
            Save these info in a safe place and do not delete them!
            Otherwise you will permanently lose these keys!
        </p>
        <textarea
            name="result-display"
            class="privatekey form-control"
            readonly="readonly"
        >{ privateKeyArmored }</textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" data-i18n="close">Close</button>
      </div>
    </div>
  </div>
</div>


<script>

this.on('update', function(e){
    if(e && (e.publicKeyArmored || e.privateKeyArmored)){
        $(this.root).find('[name="dialog-export"]').modal('show');
    }
});

</script>

</ui-pgp-keyring-export>
