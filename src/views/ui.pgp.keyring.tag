<ui-pgp-keyring>

<div class="well well-sm"><div class="pgpkeyring"></div></div>
<p>
    <div if={ !opts.popupMode }>
        <p data-i18n="you-may-want-to">You may want to ...</p>
        <div><button name="btnexport" type="button" class="btn btn-link" disabled={ selection.length < 1 } data-i18n="export-selected-keys">
            Export selected key(s)...
        </button></div>
        <div><button name="btnmanage" type="button" class="btn btn-link" disabled={ selection.length != 1 } data-i18n="modify-delete-selected-key">
            Modify or delete selected key...
        </button></div>
    </div>
    <div if={ !opts.popupMode }>
        <button name="btncompose" type="button" class="btn btn-link" disabled={ selection.length < 1} data-i18n="compose-a-message-using-keys">
            Compose a message, encrypt and/or sign it...
        </button>
    </div>
    <div if={ opts.popupMode }>
        <button name="btncompose" type="button" class="btn btn-primary" disabled={ selection.length < 1} data-i18n="choose">
            Choose
        </button>
    </div>
</p>

<ui-pgp-keyring-export></ui-pgp-keyring-export>





<script>

var self = this;
var root = this.root;
function jstreeLocate(){
    return $(root).find('.pgpkeyring');
}


var keydict = {}; // orig. key attributes corresponding to displayed item ID
self.selection = []; // always the user selected item IDs

function readKeyring(data){
    var list = [
        {
            "id": "pub",
            "parent": "#",
            "text": "Public Keys",
            "state": { "opened": true },
        },
        {
            "id": "prv",
            "parent": "#",
            "text": "Private Keys",
            "state": { "opened": true },
        },
    ];
    keydict = {};
    function escape(text){
        return $('<div>').text(text).html();
    }
    function renderKey(key){
        function seqgen(p){
            // ---- Base Item for this key.
            var baseItem = {};
            baseItem.parent = p;
            baseItem.text = "[" + key.keyId + "] " + escape(key.primaryUserid);
            baseItem.id = p + '-' + key.keyId + '-root';
            keydict[baseItem.id] = { id: key.keyId, role: p };
            list.push(baseItem);
            // ---- Key Info Entries
            // -------- All UserIds
            var uidItem = {
                "id": baseItem.id + '-uid',
                "parent": baseItem.id,
                "text": "UserIDs",
            };
            list.push(uidItem);
            for(var i in key.userIds){
                list.push({
                    "id": uidItem.id + '-' + i.toString(),
                    "parent": uidItem.id,
                    "text": escape(key.userIds[i]),
                });
            }
            // -------- All Subkeys
            var subkeyItem = {
                "id": baseItem.id + '-sub',
                "parent": baseItem.id,
                "text": "Sub keys",
            };
            list.push(subkeyItem);
            for(var i in key.subkeys){
                list.push({
                    "id": subkeyItem.id + '-' + i.toString(),
                    "parent": subkeyItem.id,
                    "text": (
                        key.subkeys[i].algorithm + ', ' +
                        key.subkeys[i].fingerprint
                    ),
                });
            };
        }
        if(key.isPublic) seqgen('pub');
        if(key.isPrivate) seqgen('prv');
    }
    data.forEach(renderKey);
    return list;
}

this.on('update', function(opts){
    if(!opts) return;
    if(opts.data){
        jstreeLocate().jstree('destroy');
        jstreeLocate().empty().jstree({
            plugins: ['wholerow', 'conditionalselect'],
            conditionalselect: function(node, event){
                return ('-root' == node.id.slice(-5));
            },
            core: { data: readKeyring(opts.data) },
        });
    }
});

this.one('mount', function(){
    jstreeLocate().click(function(){
        self.selection = jstreeLocate().jstree('get_selected');
        self.update();
    });
});



function summarizeSelection(){
    var pub = [], prv = [];
    for(var i in self.selection){
        var item = keydict[self.selection[i]];
        if(!item) continue;
        if('pub' == item.role) pub.push(item.id);
        if('prv' == item.role) prv.push(item.id);
    };
    return { pub: pub, prv: prv };
}

// on clicking the `compose` button.

$(this.btncompose).click(function(){
    var summary = summarizeSelection();
    self.trigger('compose', { encrypt: summary.pub, sign: summary.prv });
});

// on clicking the `export` button.

$(this.btnexport).click(function(){
    self.trigger('export', summarizeSelection());
});

// on clicking the `manage` button.

$(this.btnmanage).click(function(){
    self.trigger('manage', summarizeSelection());
});

</script>
</ui-pgp-keyring>
