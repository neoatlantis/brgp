<ui-preauth>

<div class="panel panel-primary">
    <div class="panel-heading">
        Preauthorized Operations
    </div>

    <div class="panel-body">
        You are about to authorize BrGP work automatically on behave of your
        name. This can be convenient but also risky. Carefully choose the
        allowed actions below.
    </div>

    <div class="list-group">
        <button
            type="button"
            class="list-group-item { auths.encrypt ? 'list-group-item-info': 'list-group-item-warning' }" 
            data-auth="encrypt" 
        >
            <h4 class="list-group-item-heading">
                Encrypt to any recipient
                :
                <span if={ auths.encrypt }>Yes</span>
                <span if={ !auths.encrypt }>No</span>
            </h4>
            <p class="list-group-item-text">
                This action authorizes BrGP generating encrypted messages to
                arbitary recipients.
            </p>
        </button>

        <button
            type="button"
            class="list-group-item { auths.sign ? 'list-group-item-info': 'list-group-item-warning' }" 
            data-auth="sign" 
        >
            <h4 class="list-group-item-heading">
                Sign arbitary message
                :
                <span if={ auths.sign }>Yes</span>
                <span if={ !auths.sign }>No</span>
            </h4>
            <p class="list-group-item-text">
                This action authorizes BrGP signing arbitary message using
                below specified private key. Be cautious with this.
            </p>
        </button>
       
        <button
            type="button"
            class="list-group-item { auths.read ? 'list-group-item-info': 'list-group-item-warning' }" 
            data-auth="read" 
        >
            <h4 class="list-group-item-heading">
                Decrypt & Verify messages received
                :
                <span if={ auths.read }>Yes</span>
                <span if={ !auths.read }>No</span>
            </h4>
            <p class="list-group-item-text">
                This action authorizes BrGP to decrypt messages you have
                received using below specified private key. Be cautious with
                this.
            </p>
        </button>
    </div>

    <div class="panel-body"><form><fieldset disabled={ !auths.sign && !auths.read }>
        <div class="form-group">
            <label>Specify a private key:</label>
            <select name="privatekey" class="form-control">
                <option each={ key in privateKeys }>{ key.primaryUserid }</option>
            </select>
        </div>
        <div class="form-group">
            <label>Give its passphrase:</label>
            <input type="password" class="form-control" />
        </div>
    </fieldset></form></div>

    <div class="panel-footer">
        <button type="button" class="btn btn-primary">
            Authorize
        </button>
    </div>

</div>





<script>

var self = this;

this.auths = {
    encrypt: false,
    sign: false,
    read: false,
}

this.on('update', function(e){
    console.log('service.preauth-ui', e); 
});

this.on('mount', function(){
    $(this.root).find('button[data-auth]').click(function(){
        var auth = $(this).attr('data-auth');
        self.auths[auth] = !self.auths[auth];
        self.update();
    });
});

</script>
</ui-preauth>
